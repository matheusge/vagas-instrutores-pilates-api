var express = require('express');
var load = require('express-load');
var app = express();

app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

module.exports = function(){
    load('route', { cwd: 'app' })
        .into(app);
	return app;
};