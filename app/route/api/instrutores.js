'use strict'

module.exports = function(app){
    app.get('/api/user', function(red, res, next){
	    res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});

        var instrutores = [
        { nome: 'Darth Vader', premium: 'NAO', email: 'darthv@gmail.com', 
            foto: 'https://i.imgflip.com/2/zhbxm.jpg', dtcadastro: '10/12/2016' },
        
        { nome: 'Luke Skywalker', premium: 'SIM', email: 'lukeskywalker@gmail.com', 
            foto: 'http://news.toyark.com/wp-content/uploads/sites/4/2016/02/SH-Figuarts-ANH-Luke-Skywalker-008-150x150.jpg', 
            dtcadastro: '11/12/2016' },

	    { nome: 'R2-D2', premium: 'SIM', email: 'r2d2@gmail.com', 
            foto: 'https://media.firebox.com/product/1760/column_grid_2/r2-d2-mobile-entertainment-system_15635.jpg', 
            dtcadastro: '11/12/2016' },

	    { nome: '', premium: 'NAO', email: 'noname@gmail.com', 
            foto: '', 
	        dtcadastro: '11/12/2016' }];

        res.end(JSON.stringify(instrutores));
	});
};
